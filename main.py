import readjsonconfig

ON = 1
OFF = 0
import sched, time

schedule = sched.scheduler(time.time, time.sleep)

def run():
    ids = readjsonconfig.get_json_list()

    query = readjsonconfig.format_gql_query(ids)

    result = readjsonconfig.make_query(query)
    store_obj_list = []

    for count, x in enumerate(result):
        r = readjsonconfig.make_store_object(result['q{}'.format(count+1)])
        store_obj_list.append(r)

    for x in store_obj_list:
        rr = readjsonconfig.should_be_open(x)

        if rr < ON:
            mutation = readjsonconfig.format_gql_mutation(x.storeid, ON, OFF)
            print(readjsonconfig.make_query(mutation))
        else:
            mutation = readjsonconfig.format_gql_mutation(x.storeid, ON, ON)
            print(readjsonconfig.make_query(mutation))



if __name__=='__main__':
    run()

    # def exe_sched(sc):
    #     print("Doing stuff...")
    #     run()
    #     schedule.enter(900, sc, exe_sched, (sc + 1,))
    #
    #
    # schedule.enter(0, 0, exe_sched, (0,))
    # schedule.run()
