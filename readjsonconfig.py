import collections
import json
from gql.transport.requests import RequestsHTTPTransport
from gql import gql, Client
from datetime import datetime, timedelta, time, date
import arrow

# VERBOSE 1, 2, 3
VERBOSE = 1


def get_json_list():
    with open("store_range.json", "r+") as fp:
        jsobj = json.load(fp)
    return jsobj['storeids']


def format_gql_mutation(storeid, enabled, isopen):
    mutation = 'mutation StoreStaus{\nupdateStoreStatus(statusInput: {storeid:' + str(storeid) + ', enabled:' + str(
        enabled) + ', isopen:' + str(isopen) + '}){\nstoreid\nname\nenabled\nisopen\n}\n}'
    if VERBOSE > 1:
        print(mutation)
    return mutation


def format_gql_query(ids):
    p1 = 'q{count}:store(storeid:{id})'

    template = """
    {
        storeid
        timezonebias
        hours{
            dow
            opens
            closes
        }
    }
    """
    body = ""

    final_gql_query = "\n{body}"
    count = 0
    for id in ids:
        count += 1
        body += p1.format(count=count, id=id) + template + '\n'

    final_gql_query = "{" + final_gql_query.format(body=body) + "}"
    return final_gql_query


def make_query(query):
    _transport = RequestsHTTPTransport(
        url='http://192.168.10.105:8000/graphql/',
        use_json=True,

    )

    client = Client(
        transport=_transport,
        fetch_schema_from_transport=True,

    )
    query = gql(query)
    return client.execute(query)


def make_store_object(dict):
    Store = collections.namedtuple('Store', ['storeid', 'timezonebias', 'dow', 'opens', 'closes'])
    return Store(dict['storeid'], dict['timezonebias'], **dict['hours'][0])


def get_today_shift(bias):
    fmt = "d"
    var = "-" + str(bias)
    now = arrow.utcnow()
    now = now.shift(hours=int(var))
    now = int(now.format(fmt))
    now += 1
    return now


def get_now_min_hour(bias):
    now = arrow.arrow.datetime.utcnow()
    now -= timedelta(hours=bias)
    return int(now.hour), int(now.minute)


def split_store_time_h_m(store):
    bias = store.timezonebias
    bias /= 60
    bias = int(bias)

    store_close_hour = int(str(store.closes)[:2])
    print("OG STORE CLOSE HOUR: ", store_close_hour)
    if VERBOSE > 0:
        print("timezonebias=", bias)
    store_close_minute = int(str(store.closes)[2:])
    store_close_minute = int(round(store_close_minute * .6, 2))
    return store_close_hour, store_close_minute, bias

# if one ret 7 else dow-1
def dow_before(dow_number):
    if dow_number > 1:
        return dow_number-1
    else:
        return 7


def should_be_open(store):
    store_close_hour, store_close_minute, timezonebias = split_store_time_h_m(store)
    now_week_day = get_today_shift(timezonebias)
    now_hour, now_minute = get_now_min_hour(timezonebias)
    if VERBOSE > 0:
        print(
            "store-dow: {}, now-dow: {}, store-close-hour: {}, now-hour: {}, store-close-minute: {}, now-minute: {}".format(
                store.dow, now_week_day, store_close_hour,
                now_hour,
                store_close_minute, now_minute))
    if store.dow == now_week_day:
        if now_hour < store_close_hour:
            if VERBOSE > 2:
                print("store-close-hour: {}, now-hour: {}, store-close-minute: {}, now-minute: {}".format(
                    store_close_hour, now_hour, store_close_minute, now_minute))
            return 1
        elif now_hour > store_close_hour:
            return 0
        elif store_close_hour == now_hour and now_minute < store_close_minute:
            return 1
        else:
            return 0
    elif now_week_day == dow_before(store.dow) and now_hour >= 17: # Logic here see dow_before() #17 will start turning on tomorrows stores at 5pm the day before
        return 1
    else:
        return 0
